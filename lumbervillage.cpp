#include <ncurses.h>
#include <string.h>
#include <pthread.h>
#include <iostream>
#include <chrono>
#include <ratio>
#include <ctime>
#include <stdlib.h>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

using namespace std;
class Tree;
class Lumber;
class Joiner;
bool program_running = true;
bool magazine_free = true;
bool magazine_free2 = true;
//vector<bool> info_printed;
vector<string> state;
vector<Tree*> trees;
vector<Lumber> lumbers;
vector<Joiner> joiners;
std::mutex mu;
std::condition_variable cv;
std::condition_variable magazine_condition;
std::condition_variable magazine_condition2;
std::condition_variable joinery_condition;
std::mutex mucv;
std::mutex magazine_mutex;
std::mutex magazine_mutex2;
std::mutex joinery_mutex;
std::mutex search_lumber_mutex;
std::condition_variable search_lumber_condition;
bool ready = false;
bool lumber_search_perm = true;
int time_counter = 1;
int help_counter = 1;
std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
int get_dead_trees();

int nature_blocks = 10;
float time_for_tick = 1.0f;
int big_wood_magazine = 0;
int small_wood_magazine = 0;
int chairs = 0;
int tables = 0;
int small_chance = 40;
int big_chance = 20;
int max_distance = 7.0f;
int max_trees = 20;
int state_blocks = 15;
int magazine_joinery_distance = 3;

char idle[] = "IDLE";


void small_wood_add(int value,int id)
{
	attron(COLOR_PAIR(7));
	small_wood_magazine+=value;
	mvprintw(9,24,"%d",small_wood_magazine);
}

void big_wood_add(int value,int id)
{
	attron(COLOR_PAIR(7));
	big_wood_magazine+=value;
	mvprintw(9,37,"%d",big_wood_magazine);
}

class Joiner
{
public:
  int id;
  float walk_speed;
  float assemble_speed;
  float magazine_speed;
  int small_wood;
  int big_wood;
  int small_wood_needed;
  int big_wood_needed;
  int counter;
  int wood_number = -1;
  bool time_taken = false;
  bool wait_printed = false;
  bool wood_taken = false;
  bool print_flag = false;
  string state;
  string destination;
  std::chrono::high_resolution_clock::time_point start_time;
  std::chrono::high_resolution_clock::time_point end_time;
  std::chrono::duration<double> elapsed_time;

  void print_needed_wood()
  {	
		mvprintw(15+id,65+state_blocks,"%s","NEEDED: S-");
		mvprintw(15+id,76+state_blocks,"%d",small_wood_needed);
		mvprintw(15+id,78+state_blocks,"%s","B-");
		mvprintw(15+id,80+state_blocks,"%d",big_wood_needed);
		
  }

  Joiner(float walk, float assemble, float magazine, string dest, int s_needed, int b_needed)
  {
	this->walk_speed = walk;
	this->assemble_speed = assemble;
	this->magazine_speed = magazine;
	this->state = "IDLE";  
	this->destination = dest;
	this->small_wood = 0;
	this->big_wood = 0;
	this->counter = 1;
	this->small_wood_needed = s_needed;
	this->big_wood_needed = b_needed;
  }

  void idle()
  {
	if((small_wood<small_wood_needed && small_wood_magazine>0)
	|| (big_wood<big_wood_needed && big_wood_magazine>0))
	{
		change_state(id,"TO MAGAZINE",id+1);	
	}
  }

  void go_to_magazine()
  {
	if(!time_taken)
	{
		time_taken = true;
		start_time = std::chrono::high_resolution_clock::now();
	}
	else if(time_taken)
	{
		end_time = std::chrono::high_resolution_clock::now();
  		elapsed_time = end_time-start_time;
		mvprintw(15+id,25,"%f",magazine_joinery_distance - elapsed_time.count()*walk_speed);
		if(elapsed_time.count()*walk_speed>=magazine_joinery_distance && magazine_free2)
		{
			std::unique_lock<std::mutex> magazine_lock2(magazine_mutex2);
			magazine_condition2.wait(magazine_lock2, []{return magazine_free2;});
			magazine_free2 = false;
			this->change_state(id,"IN MAGAZINE",id+1);
			attron(COLOR_PAIR(8));
			for(int i=0;i<state_blocks;i++)
			{
				mvprintw(3+id,23+i,"%s"," ");	
			}
			wait_printed = false;
			magazine_lock2.unlock();
		}
		else if(elapsed_time.count()*walk_speed>=magazine_joinery_distance
			 && !magazine_free2 && !wait_printed)	
		{
			wait_printed = true;
			time_taken = false;
			attron(COLOR_PAIR(8));
			mvprintw(15+id,10,"%s","           ");	
			attron(COLOR_PAIR(id+1));
			mvprintw(15+id,10,"%s","WAITING");
		}		
	}
  }

  void take_wood()
  {
	if(this->wood_taken)
	{
		if(!print_flag)
		{
		print_flag = true;
		start_time = std::chrono::high_resolution_clock::now();
		mvprintw(15+id,10,"%s","TAKING WOOD");
		attron(COLOR_PAIR(id+1));
		for(int i=0;i<state_blocks;i++)
		{
			mvprintw(15+id,25+i,"%s"," ");	
		}
		}

		end_time = std::chrono::high_resolution_clock::now();
  		elapsed_time = end_time-start_time;
		//mvprintw(25+id,10,"%f",elapsed_time.count());
		if(elapsed_time.count()>(this->magazine_speed/state_blocks)*counter)
		{
			attron(COLOR_PAIR(6));
			mvprintw(15+id,25+counter-1,"%s"," ");
			counter++;
			if(elapsed_time.count()>=this->magazine_speed)
			{
				attron(COLOR_PAIR(8));
				for(int i=0;i<state_blocks;i++)
				{
					mvprintw(15+id,25+i,"%s"," ");	
				}
				this->change_state(id,"TO JOINERY",id+1);
				magazine_free2 = true;
				magazine_condition2.notify_one();
				print_flag = false;
				counter = 1;
				
			}
		}	
	}
	else if(this->small_wood<this->small_wood_needed && small_wood_magazine>0)
	{
		small_wood_add(-1, id+1);
		this->wood_taken = true;
		this->wood_number = 1;
	}
	else if(this->big_wood<this->big_wood_needed && big_wood_magazine>0)
	{
		big_wood_add(-1,id+1);
		this->wood_taken = true;
		this->wood_number = 2;
	}
	
  }

  void go_to_joinery()
  {
	if(!time_taken)
	{
		time_taken = true;
		start_time = std::chrono::high_resolution_clock::now();
	}
	else if(time_taken)
	{
		end_time = std::chrono::high_resolution_clock::now();
  		elapsed_time = end_time-start_time;
		mvprintw(15+id,25,"%f",magazine_joinery_distance - elapsed_time.count()*walk_speed);
		if(elapsed_time.count()*walk_speed>=magazine_joinery_distance)
		{
			this->change_state(id,"PLACING WOOD",id+1);
			wait_printed = false;
			time_taken = false;
		}		
	}
  }

  void place_wood()
  {
	if(!print_flag)
	{
	print_flag = true;
	start_time = std::chrono::high_resolution_clock::now();
	for(int i=0;i<state_blocks;i++)
	{
		mvprintw(15+id,25+i,"%s"," ");	
	}
	}

	end_time = std::chrono::high_resolution_clock::now();
  	elapsed_time = end_time-start_time;
	//mvprintw(10+id,23,"%d",this->counter);
	if(elapsed_time.count()>(magazine_speed/state_blocks)*counter)
	{
		attron(COLOR_PAIR(6));
		mvprintw(15+id,25+counter-1,"%s", " ");
		counter++;
		if(elapsed_time.count()>=magazine_speed)
		{
			print_flag = false;
			counter = 1;
			wood_taken = false;
			if(this->wood_number == 1)
			{
			this->small_wood++;
			attron(COLOR_PAIR(id+1));
			mvprintw(15+id,38+state_blocks,"%d",this->small_wood);
			}
			else if(this->wood_number == 2)
			{
			this->big_wood++;
			attron(COLOR_PAIR(id+1));
			mvprintw(15+id,51+state_blocks,"%d",this->big_wood);
			}
			attron(COLOR_PAIR(8));
				for(int i=0;i<state_blocks;i++)
				{
					mvprintw(15+id,25+i,"%s"," ");	
				}
			if(small_wood<small_wood_needed || big_wood<big_wood_needed)
			{
				this->change_state(id,"IDLE",id+1);
			}
			else
			{
				this->change_state(id,"PRODUCING",id+1);
			}
		}
	}
  }

  void produce()
  {
	if(!print_flag)
	{
	print_flag = true;
	start_time = std::chrono::high_resolution_clock::now();
	for(int i=0;i<state_blocks;i++)
	{
		mvprintw(15+id,25+i,"%s"," ");	
	}
	this->small_wood = 0;
	mvprintw(15+id,38+state_blocks,"%d",this->small_wood);
	this->big_wood = 0;
	mvprintw(15+id,51+state_blocks,"%d",this->big_wood);
	}

	end_time = std::chrono::high_resolution_clock::now();
  	elapsed_time = end_time-start_time;
	if(elapsed_time.count()>(assemble_speed/state_blocks)*counter)
	{
		attron(COLOR_PAIR(7));
		mvprintw(15+id,25+counter-1,"%s", " ");
		counter++;
		if(elapsed_time.count()>=assemble_speed)
		{
			print_flag = false;
			counter = 1;
			attron(COLOR_PAIR(8));
			for(int i=0;i<state_blocks;i++)
			{
				mvprintw(15+id,25+i,"%s"," ");	
			}
			attron(COLOR_PAIR(7));
			if(this->destination == "CHAIRS")
			{
				chairs++;
  				mvprintw(20,20,"%d",chairs);
			}
			else if(this->destination == "TABLES")
			{
				tables++;
  				mvprintw(20,35,"%d",tables);
			}
			this->change_state(id,"IDLE",id+1);
		}
		
	}
  }

  void change_state(int index, string st, int color)
  {
	attron(COLOR_PAIR(8));
	mvprintw(15+index,10,"%s","             ");
	attron(COLOR_PAIR(color));
	this->state = st;
	//mu.lock();
	mvprintw(15+index,10,"%s",state.c_str());
	//mu.unlock();  
  }

};

void carpenter_function(int index)
{
  mu.lock();
  joiners.at(index).id = index;
  attron(COLOR_PAIR(index+1));
  mvprintw(15+index,0,"%s","JOINER");
  mvprintw(15+index,7,"%d",index+1);
  mvprintw(15+index,10,"%s",joiners.at(index).state.c_str());
  attron(COLOR_PAIR(8));
  for(int i=0;i<state_blocks;i++)
  {
	mvprintw(15+index,25+i,"%s"," ");	
  }
  attron(COLOR_PAIR(index+1));
  mvprintw(15+index,26+state_blocks,"%s","SMALL WOOD:");
  mvprintw(15+index,38+state_blocks,"%d",joiners.at(index).small_wood);
  mvprintw(15+index,41+state_blocks,"%s","BIG WOOD:");
  mvprintw(15+index,51+state_blocks,"%d",joiners.at(index).big_wood);
  mvprintw(15+index,56+state_blocks,"%s",joiners.at(index).destination.c_str());
  joiners.at(index).print_needed_wood();
  mu.unlock();

  while(program_running)
  {
	mu.lock();
	attron(COLOR_PAIR(index+1));

	if(joiners.at(index).state == "IDLE")
	{
		joiners.at(index).idle();
	}
	else if(joiners.at(index).state == "TO MAGAZINE")
	{
		joiners.at(index).go_to_magazine();
	}
	else if(joiners.at(index).state == "IN MAGAZINE")
	{
		joiners.at(index).take_wood();
	}
	else if(joiners.at(index).state == "TO JOINERY")
	{
		joiners.at(index).go_to_joinery();
	}
	else if(joiners.at(index).state == "PLACING WOOD")
	{
		joiners.at(index).place_wood();
	}
	else if(joiners.at(index).state == "PRODUCING")
	{
		joiners.at(index).produce();
	}
	
	mu.unlock();  
  }
}



class Tree
{
public:
  static int small_trees;
  static int big_trees;
  float thickness;
  int distance;
  bool busy;
  bool dead;
  Tree(float thickness, int distance)
  {
	this->thickness = thickness;
	this->distance = distance;
	this->busy = false;
	this->dead = false;
	if(thickness>=0 && thickness<=5)
	{
		small_trees++;
	}
	if(thickness>=6 && thickness<=10)
	{
		big_trees++;
	}
  }

  ~Tree()
  {
	if(thickness>=0 && thickness<=5)
	{
		small_wood_add(1,7);
	}
	if(thickness>=6 && thickness<=10)
	{
		big_wood_add(1,7);
	}
  }

  void print_focus(int index)
  {
	mvprintw(3+index,25+state_blocks,"%s","FOCUSED TREE");
	mvprintw(3+index,38+state_blocks,"%s","THICKNESS:");
	mvprintw(3+index,48+state_blocks,"%d",(int)this->thickness);
	mvprintw(3+index,52+state_blocks,"%s","DISTANCE:");
	mvprintw(3+index,62+state_blocks,"%d",this->distance);
  }

};

int get_dead_trees()
{
	int counter = 0;
	for(int i=0;i<trees.size();i++)
	{
		if(trees.at(i)->dead)
		{
			counter++;
		}
	}
	return counter;
}

int search_counter=0;
int search_for_free_trees()
{
	search_counter++;
	if(trees.size()==0)
	{
	return -1;
	}
	//mvprintw(10,5,"%s", "TEST");
	for(int i=0;i<trees.size();i++)
	{
		if(trees.at(i)->busy == false && trees.at(i)->dead == false)
		{
			//mvprintw(10,5,"%s", "TEST");
			return i;
		}
	}
	return -1;
}

class Lumber
{
public:
float walk_speed;
float cut_speed;
float magazine_speed;
int id;
int counter;
string state;
bool time_taken;
bool print_flag = false;
bool wait_printed = false;
std::chrono::high_resolution_clock::time_point start_time;
std::chrono::high_resolution_clock::time_point end_time;
std::chrono::duration<double> elapsed_time;
Tree *focus;

  Lumber(float a, float b, float c)
  {
	this->walk_speed = a;
	this->cut_speed = b;
	this->magazine_speed = c;
	this->state = "IDLE";
	this->focus = NULL;
	this->time_taken = false;
	this->counter = 1;
	
  }

  void change_state(int index, string st, int color)
  {
	attron(COLOR_PAIR(color));
	this->state = st;
	//mu.lock();
	mvprintw(3+index,11,"%s",state.c_str());
	//mu.unlock();  
  }

  void idle()
  {
		attron(COLOR_PAIR(8));
		mvprintw(3+id,25+state_blocks,"%s","                                         ");
  		int ind = search_for_free_trees();
		if(this->focus==NULL && ind!=-1)
		{
			std::unique_lock<std::mutex> search_lock(search_lumber_mutex);	//CRIT
			search_lumber_condition.wait(search_lock, []{return lumber_search_perm;});
			
			lumber_search_perm = false;
			trees.at(ind)->busy = true;
			this->focus = trees.at(ind);
			//mvprintw(3+index,20,"%d",lumbers.at(index).focus->distance);
			this->change_state(id,"TO TREE",id+1);
			this->focus->print_focus(id);

			search_lock.unlock();
			lumber_search_perm = true;
			search_lumber_condition.notify_one();
		}
  }	

  void go_to_tree()
  {
	if(!time_taken)
	{
		time_taken = true;
		start_time = std::chrono::high_resolution_clock::now();
	}
	else if(time_taken && focus!=NULL)
	{
		end_time = std::chrono::high_resolution_clock::now();
  		elapsed_time = end_time-start_time;
		mvprintw(3+id,23,"%f",this->focus->distance - elapsed_time.count()*walk_speed);
		//mvprintw(4+id,23,"%d",this->focus->distance);
		//mvprintw(5+id,23,"%f",this->focus->thickness);
		//mvprintw(6+id,19,"%s",this->state.c_str());
		if(elapsed_time.count()*walk_speed>=this->focus->distance)
		{
			this->change_state(id,"CUTTING",id+1);
			time_taken = false;
		}
		//mvprintw(6+id,19,"%d",this->focus->busy);
		
	}
  }

  void cut_tree()
  {
	if(!print_flag)
	{
	print_flag = true;
	start_time = std::chrono::high_resolution_clock::now();
	attron(COLOR_PAIR(id+1));
	for(int i=0;i<state_blocks;i++)
	{
		mvprintw(3+id,23+i,"%s"," ");	
	}
	}

	end_time = std::chrono::high_resolution_clock::now();
  	elapsed_time = end_time-start_time;
	//mvprintw(10+id,23,"%d",this->counter);
	if(elapsed_time.count()*cut_speed>(this->focus->thickness/state_blocks)*counter)
	{
		attron(COLOR_PAIR(6));
		mvprintw(3+id,23+counter-1,"%s"," ");
		counter++;
		if(elapsed_time.count()*cut_speed>=this->focus->thickness)
		{
			this->change_state(id,"TO MAGAZINE",id+1);
			this->focus->dead = true;
			print_flag = false;
			counter = 1;
			attron(COLOR_PAIR(8));
			for(int i=0;i<state_blocks;i++)
			{
				mvprintw(3+id,23+i,"%s"," ");	
			}
		}
	}	
  }
  void go_to_magazine()
  {
	if(!time_taken)
	{
		time_taken = true;
		start_time = std::chrono::high_resolution_clock::now();
	}
	else if(time_taken)
	{
		end_time = std::chrono::high_resolution_clock::now();
  		elapsed_time = end_time-start_time;
		mvprintw(3+id,23,"%f",this->focus->distance - elapsed_time.count()*walk_speed);
		//mvprintw(4+id,23,"%d",this->focus->distance);
		//mvprintw(5+id,23,"%f",this->focus->thickness);
		//mvprintw(6+id,19,"%s",this->state.c_str());
		if(elapsed_time.count()*walk_speed>=this->focus->distance && magazine_free)
		{
			std::unique_lock<std::mutex> magazine_lock(magazine_mutex);
			magazine_condition.wait(magazine_lock, []{return magazine_free;});
			magazine_free = false;
			this->change_state(id,"IN MAGAZINE",id+1);
			attron(COLOR_PAIR(8));
			for(int i=0;i<state_blocks;i++)
			{
				mvprintw(3+id,23+i,"%s"," ");	
			}
			wait_printed = false;
			magazine_lock.unlock();
		}
		else if(elapsed_time.count()*walk_speed>=this->focus->distance
			 && !magazine_free && !wait_printed)	
		{
			wait_printed = true;
			attron(COLOR_PAIR(8));
			mvprintw(3+id,11,"%s","           ");	
			attron(COLOR_PAIR(id+1));
			mvprintw(3+id,11,"%s","WAITING");
		}
		//mvprintw(6+id,19,"%d",this->focus->busy);
		
	}
  }
  void place_wood()
  {
	if(!print_flag)
	{
	print_flag = true;
	start_time = std::chrono::high_resolution_clock::now();
	for(int i=0;i<state_blocks;i++)
	{
		mvprintw(3+id,23+i,"%s"," ");	
	}
	}

	end_time = std::chrono::high_resolution_clock::now();
  	elapsed_time = end_time-start_time;
	//mvprintw(10+id,23,"%d",this->counter);
	if(elapsed_time.count()>(magazine_speed/state_blocks)*counter)
	{
		attron(COLOR_PAIR(7));
		mvprintw(3+id,23+counter-1,"%s", " ");
		counter++;
		if(elapsed_time.count()>=magazine_speed)
		{
			magazine_free = true;
			magazine_condition.notify_one();

			delete this->focus;
			this->focus = NULL;
			print_flag = false;
			counter = 1;
			attron(COLOR_PAIR(8));
			this->change_state(id,"           ",8);
			for(int i=0;i<state_blocks;i++)
			{
				mvprintw(3+id,23+i,"%s"," ");	
			}
			attron(COLOR_PAIR(id+1));
			this->change_state(id,"IDLE",id+1);
		}
	}
  }

};

int Tree::small_trees = 0;
int Tree::big_trees = 0;

void print_function(int index)
{
	std::unique_lock<std::mutex> lock(mucv);
	
	cv.wait(lock, []{return ready;});
	
	//mu.lock();
	attron(COLOR_PAIR(index));
	mvprintw(10+help_counter,index,"%s","PRINT");
	//mu.unlock();

	help_counter++;
	if(program_running)
	{
		ready = false;
	}
	lock.unlock();

}

void nature_tick()
{
  if(trees.size()<max_trees)
  {
	int random = rand()% 100+1;
	if(random<=small_chance)
	{
		int rand_thickness = rand()%5+1;
		int rand_dist = rand()%max_distance+1;
		Tree *temp = new Tree(rand_thickness,rand_dist);
		trees.push_back(temp);
		attron(COLOR_PAIR(6));
		mu.lock();
		mvprintw(0,0+nature_blocks+14,"%d",Tree::small_trees);
		mu.unlock();
	}
  }
  
  if(trees.size()<max_trees)
  {
	int random = rand()% 100+1;
	if(random<=big_chance)
	{
		int rand_thickness = rand()%5+6;
		int rand_dist = rand()%max_distance+1;
		Tree *temp = new Tree(rand_thickness,rand_dist);
		trees.push_back(temp);
		attron(COLOR_PAIR(6));
		mu.lock();
		mvprintw(0,0+nature_blocks+28,"%d",Tree::big_trees);
		mu.unlock();
	}
  }
	//mvprintw(15,15,"%s",lumbers.at(0).state);

}

void input_function()
{  

  while(program_running)
  {

	  if(getch()=='q')
	  {
		
		//mvprintw(5,5,"%s", "QQQQ");
		ready = true;
		mu.unlock();
		program_running = false;
		cv.notify_all();
		//mu.unlock();
		//lock.unlock();
		
		
	  }
	  if(getch()=='5')
	  {
		max_trees+=5;
    	  }
  }
}

void lumber_function(int index)
{

  mu.lock();
  lumbers.at(index).id = index;
  attron(COLOR_PAIR(index+1));
  mvprintw(3+index,0,"%s","LUMBER");
  mvprintw(3+index,7,"%d",index+1);
  mvprintw(3+index,11,"%s",idle);
  mu.unlock();
  while(program_running)
  {
	mu.lock();
	attron(COLOR_PAIR(index+1));
	//print_function(index);
	//mvprintw(8+index,19,"%d",search_counter);
	//mvprintw(16+index,15,"%s",lumbers.at(index-1).state);

	if(lumbers.at(index).state == "IDLE")
	{
		lumbers.at(index).idle();
	}
	else if(lumbers.at(index).state == "TO TREE")
	{
		lumbers.at(index).go_to_tree();
	}
	else if(lumbers.at(index).state == "CUTTING")
	{
		lumbers.at(index).cut_tree();
	}
	else if(lumbers.at(index).state == "TO MAGAZINE")
	{
		lumbers.at(index).go_to_magazine();
	}
	else if(lumbers.at(index).state == "IN MAGAZINE")
	{
		lumbers.at(index).place_wood();
	}
	//mvprintw(15+index,0,"%s",lumbers.at(index).state);

	mu.unlock();  
  }
}

void nature_function()
{
	mu.lock();
	attron(COLOR_PAIR(1));
	for(int i=0;i<nature_blocks;i++)
	{
		mvprintw(0,0+i,"%s"," ");	
	}
	attron(COLOR_PAIR(6));
	mvprintw(0,0+nature_blocks+1,"%s","SMALL TREES:");
	mvprintw(0,0+nature_blocks+17,"%s","BIG TREES:");
	mvprintw(0,0+nature_blocks+14,"%d",Tree::small_trees);
	mvprintw(0,0+nature_blocks+28,"%d",Tree::big_trees);
	mu.unlock();
	
  	while(program_running)
	{
		//mu.lock();
		//mvprintw(20,0,"%d",trees.size());		
		//mu.unlock();
		auto end = std::chrono::high_resolution_clock::now();
  		std::chrono::duration<double> elapsed = end-start;
  		//mvprintw(1,0,"%f", elapsed.count());
		if(elapsed.count()>(time_for_tick/nature_blocks)*time_counter)
		{	
			
			mu.lock();
			attron(COLOR_PAIR(2));
			mvprintw(0,-1+time_counter,"%s", " ");
			mu.unlock();
			time_counter++;
			if(time_counter>=nature_blocks)
			{
			start = std::chrono::high_resolution_clock::now();
			
			for(int i=0;i<nature_blocks;i++)
			{
				mu.lock();
				attron(COLOR_PAIR(1));
				mvprintw(0,0+i,"%s"," ");
				mu.unlock();	
			}
			time_counter=1;
			nature_tick();
			//ready = true;
			//cv.notify_one();		
			}
		}


		
		refresh();
		
	}
}

int main ()
{
  srand(time(NULL));
  initscr();
  start_color();
  init_pair(1, COLOR_BLACK, COLOR_RED);
  init_pair(2, COLOR_BLACK, COLOR_GREEN);
  init_pair(3, COLOR_BLACK, COLOR_BLUE);
  init_pair(4, COLOR_BLACK, COLOR_YELLOW);
  init_pair(5, COLOR_BLACK, COLOR_CYAN);
  init_pair(6, COLOR_BLACK, COLOR_MAGENTA);
  init_pair(7, COLOR_BLACK, COLOR_WHITE);
  init_pair(8, COLOR_BLACK, COLOR_BLACK);

  lumbers.push_back(Lumber(1.0f,1.0f,5.0f));  
  lumbers.push_back(Lumber(1.0f,1.0f,5.0f));
  lumbers.push_back(Lumber(1.0f,1.0f,5.0f));
  lumbers.push_back(Lumber(1.0f,1.0f,5.0f));
  lumbers.push_back(Lumber(1.0f,1.0f,5.0f));

  joiners.push_back(Joiner(1.0f,1.0f,1.0f,"CHAIRS",2,2));
  joiners.push_back(Joiner(1.0f,1.0f,1.0f,"TABLES",3,3));
  
  attron(COLOR_PAIR(7));
  mvprintw(9,0,"%s","MAGAZINE");
  mvprintw(9,12,"%s","SMALL WOOD:");
  mvprintw(9,24,"%d",small_wood_magazine);
  mvprintw(9,27,"%s","BIG WOOD:");
  mvprintw(9,37,"%d",big_wood_magazine);

  mvprintw(20,0,"%s","JOINERY");
  mvprintw(20,12,"%s","CHAIRS:");
  mvprintw(20,20,"%d",chairs);
  mvprintw(20,27,"%s","TABLES:");
  mvprintw(20,35,"%d",tables);

  string idlestring = "IDLE";
  for(int i=0;i<5;i++)
  {
	//info_printed.push_back(false);
	//mvprintw(5+i,20,"%s","|");
	state.push_back(idlestring);

  }

  std::thread input(input_function);
  std::thread nature(nature_function);

  std::thread lumber1(lumber_function, 0);  
  std::thread lumber2(lumber_function, 1);  
  std::thread lumber3(lumber_function, 2);  
  std::thread lumber4(lumber_function, 3);  
  std::thread lumber5(lumber_function, 4);  

  std::thread carpenter1(carpenter_function, 0);
  std::thread carpenter2(carpenter_function, 1);
 
 input.join();
 nature.join(); 

 lumber1.join();
 lumber2.join();
 lumber3.join();
 lumber4.join();
 lumber5.join();

 carpenter1.join();
 carpenter2.join();

refresh();
endwin();
return 0;
}







